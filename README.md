# 25 Light Years

A visualization of the stars in a 25 light year radius around our sun.

## Purpose

This was mostly a project to bring together several MacOS development goals I've been working on over the previous year: Parsing files, working with data tables, importing 3D objects from Blender, creating textures from bitmaps and rendering 3D scenes with Metal. 

I'm publishing this not so much as a polished demo of Metal rendering and more as a resource for myself and others for a compile-ready, working example of these techniques in combination.

Screenshot:

![A picture of the app running. These are the approximately 200 stars in our neighborhood](./screenshot.png){width=40%}

Youtube Video:

[![](http://img.youtube.com/vi/zfYiUjZ3BkI/0.jpg)](https://www.youtube.com/embed/zfYiUjZ3BkI "25 Light Years Animation")

## Structure

The project consists of several components:

- a CSV file with a list of the approximately 200 stars in a 25 light year radius around the sun

- ContentView.swift setting up a MetalKit renderer view

- globals.swift file setting up the necessary data structures and the CSV importer

- Renderer.swift doing much of the heavy lifting: loading the 3d objects, setting up the scene and animating everything

- Shaders.metal is a simple 3D shader for the scene. We use three lights to evenly light the objects, but this is really just a choice, this shader is under-utilized and can produce much nicer looking results

## To-do

I'm not happy how the text labels came out at the end... I didn't realize for a long time that I wouldn't be able to create a bitmap texture for a flat 3D object with transparent backgrounds - so the rectangles are black with red text, which does look weird when the stars are rotating around the position of the sun. At some point I want to go back and do something different, where the letters are actual individual 3D objects, but that is a bit of a different beast altogether. 

## Distribution

This project should load and compile in Xcode without errors. It does not rely on any 3rd party libraries and should work out of the box. It was developed and tested on Mac OS 13 (Ventura) with Xcode 14.2

## License

This code is essentially free. Much of it was cobbled together from countless tutorials, Stack Overflow answers, blog posts, Youtube videos and random code snippets found during desperate late-night searches for working MetalKit examples. Thanks to everybody who contributed to any of these sources.