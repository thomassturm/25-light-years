
import Foundation
import MetalKit
import simd
import CoreImage
import CoreImage.CIFilterBuiltins       // this helps with autocomplete filter properties in the editor


class Renderer: NSObject, MTKViewDelegate {
    let device: MTLDevice
    let commandQueue: MTLCommandQueue
    var renderPipeline: MTLRenderPipelineState
    let depthStencilState: MTLDepthStencilState
    let samplerState: MTLSamplerState
    var vertexDescriptor: MDLVertexDescriptor
    
    var baseColorTexture: MTLTexture?
    var rawTexture = [UInt8](repeating: 0, count: 16 * 16 * 4)
    var colors = [CGColor](repeating: CGColor(red:1,green:0,blue:0,alpha:255), count: 16)
    let bitsPerComponent = Int(8)

    var context: CIContext!

    let converterContext = CIContext(options: nil)
    
    var currentFilter: CIFilter!
    
    var time: Float = 0
    var nodeCounter: Int = 0

    let scene: Scene
    var projectionMatrix = matrix_identity_float4x4
    var viewMatrix = matrix_identity_float4x4
    var cameraWorldPosition = SIMD3<Float>(0, 0, 0)

    
    init(view: MTKView, device: MTLDevice) {
        getCSVData()    // this fills the AllStars array with the data for the stars

        logger(logmsg: "Number of Stars: " + String(AllStars.count))

        self.device = device
        commandQueue = device.makeCommandQueue()!
        vertexDescriptor = Renderer.buildVertexDescriptor()

        self.scene = Renderer.buildScene(device: device, vertexDescriptor: vertexDescriptor)
        renderPipeline = Renderer.buildPipeline(device: device, view: view, vertexDescriptor: vertexDescriptor)
        samplerState = Renderer.buildSamplerState(device: device)
        depthStencilState = Renderer.buildDepthStencilState(device: device)

        super.init()

        initColors()        // set up the color table for different star types

        // now add the models and textures for the stars to the asset store
        var tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starBlank", baseColor: 0)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starWhiteDwarf", baseColor: 0)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starBrownDwarf", baseColor: 1)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starWhite", baseColor: 2)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starYellowWhite", baseColor: 3)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starYellow", baseColor: 4)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starOrange", baseColor: 5)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "star_blank", tURL: "", name: "starRedOrange", baseColor: 6)
        NodeAssetStore.append(tmpNode)
        tmpNode = importNode(mURL: "screen", tURL: "", name: "emptyRect", baseColor: 1)     // this is an empty rectangle, we'll use this for the name labels for each star
        NodeAssetStore.append(tmpNode)

        // create the list of 3D objects and the scene node list
        addStarsToRenderer()
    }

    
    func addStarsToRenderer() {
        for i in 0...(AllStars.count-1) {
            let aX = Float(AllStars[i].X)!
            let aY = Float(AllStars[i].Y)!
            let aZ = Float(AllStars[i].Z)!
            let tmpScale = Float(AllStars[i].RADIUS) ?? 0.5
            let aS = (tmpScale) / (1 + tmpScale/2)              // change this for global size scale changes
            let aA = Float(0)
            let aR = Float(0)
            
            // pick one of the assets from the asset store based on the original star type classification
            var assetName = "starBlank"
            if (AllStars[i].TY.contains("WD")) {
                assetName = "starWhiteDwarf"
            }
            if (AllStars[i].TY.contains("BD")) {
                assetName = "starBrownDwarf"
            }
            if (AllStars[i].TY.contains("S") && AllStars[i].SPEC.contains("A")) {
                assetName = "starWhite"
            }
            if (AllStars[i].TY.contains("S") && AllStars[i].SPEC.contains("F")) {
                assetName = "starYellowWhite"
            }
            if (AllStars[i].TY.contains("S") && AllStars[i].SPEC.contains("G")) {
                assetName = "starYellow"
            }
            if (AllStars[i].TY.contains("S") && AllStars[i].SPEC.contains("K")) {
                assetName = "starOrange"
            }
            if (AllStars[i].TY.contains("S") && AllStars[i].SPEC.contains("M")) {
                assetName = "starRedOrange"
            }

            let renderNodeName = addNodeToScene(assetName: assetName, rX: aX, rY: aY, rZ: aZ, rS: aS, rA: aA)
//            let aNode = findNodeByName(renderNodeName)    // this is how we can find the node in the render list

            // store the name of the object in the render list and any properties we want to manipulate in our array of objects
            // this allows us to have objects in the render list that we will never manipulate
            let a = SpaceObject(
                nodeName: String(renderNodeName),
                rotation: Float(aR),
                size: Float(aS),
                type: "Star"
            )
            SpaceObjects.append(a)
            
            if AllStars[i].NAME != "" {         // if this entry in the star list has a name, we create a lable to float above the star
                
                // create the colors for the text label
                let backgroundColor = CIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
                let textColor = CIColor.init(red: 0.4, green: 0, blue: 0, alpha: 1)
                let backNSColor = NSColor(ciColor: backgroundColor)
                let textNSColor = NSColor(ciColor: textColor)

                // text attributes
                let attributes = [
                  NSAttributedString.Key.backgroundColor : backNSColor,
                  NSAttributedString.Key.foregroundColor : textNSColor,
                  NSAttributedString.Key.font : NSFont(name: "HelveticaNeue", size: 128)!
                ]
                let starLabelText = NSAttributedString(
                        string: AllStars[i].NAME,
                        attributes: attributes
                )
                let textFilter = CIFilter.attributedTextImageGenerator()
                textFilter.text = starLabelText
                textFilter.scaleFactor = 4.0
                // textFilter.outputImage is now the texture containing the name of the star as a bitmap
                
                // create the rectangle for this name label
                var tmpNode:Node? = nil
                for a in NodeAssetStore {
                    if a.name == "emptyRect" {  // find the empty rectangle base model
                        tmpNode = a
                    }
                }
                let newNode = Node(name: "V" + String(nodeCounter))
                nodeCounter = nodeCounter + 1
                newNode.mesh = tmpNode?.mesh        // copy the rectangle mesh into a new 3D node
                
                let textureSizeX = textFilter.outputImage!.extent.width
                let textureSizeY = textFilter.outputImage!.extent.height

                let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.rgba8Unorm, width: Int(textureSizeX), height: Int(textureSizeY), mipmapped: false)

                newNode.material.baseColorTexture = device.makeTexture(descriptor: textureDescriptor)

                // now create a bitmap of the text
                guard let outputCGImage = converterContext.createCGImage(textFilter.outputImage!, from: textFilter.outputImage!.extent) else { return }

                let options: [MTKTextureLoader.Option : Any] = [
                    .origin : MTKTextureLoader.Origin.bottomLeft.rawValue
                ]
                
                // create a texture for the Metal Renderer from the bitmap
                let textureLoader = MTKTextureLoader(device: device)
                newNode.material.baseColorTexture = try? textureLoader.newTexture(cgImage: outputCGImage, options: options)
                newNode.material.specularPower = 0
//                newNode.material.specularColor = SIMD3<Float>(0.05, 0.05, 0.05)

                // now our newNode has the name bitmap as texture, add it to the render list
                // scale all text labels
                let aSLabel:Float = 0.6
                
                newNode.modelMatrix = float4x4(translationBy: SIMD3<Float>(aX+1.5, aY+1, aZ)) * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: aA) *  float4x4(scaleBy: aSLabel)
                newNode.s = aSLabel
                newNode.a = aA          // the initial angle of the label is 0, facing the camera
                newNode.x = aX+1.5      // slightly to the right
                newNode.y = aY+1        // and above the star object
                newNode.z = aZ
                newNode.vx = 0
                newNode.vy = 0
                newNode.vz = 0
                scene.rootNode.children.append(newNode)
                
                // now we add this new node to our list of objects we want to manipulate on every render loop
                let a = SpaceObject (
                    nodeName: String(newNode.name),
                    rotation: Float(0.00845),               // this is the speed of the rotation per render draw cycle
                    size: Float(aSLabel),
                    type: "Label"
                )
                SpaceObjects.append(a)
            }
            
            
            
        }   // end for i
    }
    
    
    // this method adds the model from the asset store as a new node to the render list; it returns the internal name of the node in the render list
    func addNodeToScene(assetName: String, rX: Float, rY: Float, rZ: Float, rS: Float, rA: Float) -> String {
        var tmpNode:Node? = nil
        for a in NodeAssetStore {
            if a.name == assetName {
                tmpNode = a
            }
        }

        let nodeName = "V" + String(nodeCounter)    // nodes in the scene use a running number prefixed by "V" as names
        
        let newNode = Node(name: nodeName)    // we now give out permanent names for nodes
        nodeCounter = nodeCounter + 1
        newNode.mesh = tmpNode?.mesh
        newNode.material = tmpNode!.material
        newNode.modelMatrix = float4x4(translationBy: SIMD3<Float>(rX, rY, rZ)) * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: rA) *  float4x4(scaleBy: rS)
        newNode.s = rS
        newNode.a = rA
        newNode.x = rX
        newNode.y = rY
        newNode.z = rZ
        newNode.vx = 0
        newNode.vy = 0
        newNode.vz = 0
        scene.rootNode.children.append(newNode)
        return nodeName
    }
    
    
    // this method imports the Blender model and the texture UV map; if no texture name is given, it creates a synthetic texture with a base color from the colors array
    func importNode(mURL: String, tURL: String, name: String, baseColor: Int) -> Node {
        let modelURL = Bundle.main.url(forResource: mURL, withExtension: "obj")!
        let tmpNode = loadModelMesh(modelURL: modelURL, device: device)
        tmpNode.name = name     // this is the name used to retrieve a preloaded model from the nodeAssetStore

        if (tURL != "") {           // we've got a texture to load
            // import a texture
            let textureLoader = MTKTextureLoader(device: device)
            let origin = NSString(string: MTKTextureLoader.Origin.bottomLeft.rawValue)
            let options: [MTKTextureLoader.Option : Any] = [.generateMipmaps : true, .SRGB : true, .origin : origin]
            tmpNode.material = Material()
            tmpNode.material.baseColorTexture = try? textureLoader.newTexture(name: tURL, scaleFactor: 1.0, bundle: nil, options: options)
            tmpNode.material.specularPower = 1
            tmpNode.material.specularColor = SIMD3<Float>(0.05, 0.05, 0.05)
        } else {                    // we need to make a texture
            // create a pure color texture from a color value
            let rgbColorSpace = CGColorSpace(name: CGColorSpace.genericRGBLinear)!
            let bitmapInfo = CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue
            let context = CGContext(data: &rawTexture, width: Int(16), height: Int(16), bitsPerComponent: bitsPerComponent, bytesPerRow: 16*4, space: rgbColorSpace, bitmapInfo: bitmapInfo)!
            //logger(logmsg: "creating voxel with color "+String(spectrumColor))
            if baseColor != -1 {
                context.setFillColor(colors[baseColor])
                context.fill(CGRect(x: 0, y: 0, width: CGFloat(16), height: CGFloat(16)))
                
                let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.rgba8Unorm, width: Int(16), height: Int(16), mipmapped: false)
                
                let tmpMaterial = Material()
                
                tmpMaterial.baseColorTexture = device.makeTexture(descriptor: textureDescriptor)
                let region = MTLRegionMake2D(0, 0, Int(16), Int(16))
                tmpMaterial.baseColorTexture?.replace(region: region, mipmapLevel: 0, withBytes: &rawTexture, bytesPerRow: Int(16*4))
                
                tmpMaterial.specularPower = 100
                tmpMaterial.specularColor = SIMD3<Float>(0.1, 0.1, 0.1)
                tmpNode.material = tmpMaterial
            }
        }
        return tmpNode
    }

    
    func loadModelMesh(modelURL: URL, device: MTLDevice) -> Node {
        let vertexDescriptor = MDLVertexDescriptor()
        vertexDescriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition, format: .float3, offset: 0, bufferIndex: 0)
        vertexDescriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal, format: .float3, offset: MemoryLayout<Float>.size * 3, bufferIndex: 0)
        vertexDescriptor.attributes[2] = MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate, format: .float2, offset: MemoryLayout<Float>.size * 6, bufferIndex: 0)
        vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * 8)
        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        let modelNode = Node(name: "tmp")
        let modelAsset = MDLAsset(url: modelURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)
        modelNode.mesh = try! MTKMesh.newMeshes(asset: modelAsset, device: device).metalKitMeshes.first!
        return modelNode
    }

    
    func initColors() {
        colors[0] = setColor(rgb: 0xd0d8ff) // White Dwarf
        colors[1] = setColor(rgb: 0x502000) // Brown Dwarf
        colors[2] = setColor(rgb: 0xf0f0f0) // White
        colors[3] = setColor(rgb: 0xfff0a0) // Yellow-White
        colors[4] = setColor(rgb: 0xf0f033) // Yellow
        colors[5] = setColor(rgb: 0xf0b000) // Orange
        colors[6] = setColor(rgb: 0x907033) // Red-Orange
    }


    // create a CGColor from a 3-byte hex value
    func setColor(rgb: Int) -> CGColor {
        let newColor = CGColor(
            red: CGFloat(Float((rgb >> 16) & 0xFF)/255),
            green: CGFloat(Float((rgb >> 8) & 0xFF)/255),
            blue: CGFloat(Float(rgb & 0xFF)/255),
            alpha: 1
        )
        return newColor
    }
    
    
    static func buildVertexDescriptor() -> MDLVertexDescriptor {
        let vertexDescriptor = MDLVertexDescriptor()
        vertexDescriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition,
                                                            format: .float3,
                                                            offset: 0,
                                                            bufferIndex: 0)
        vertexDescriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal,
                                                            format: .float3,
                                                            offset: MemoryLayout<Float>.size * 3,
                                                            bufferIndex: 0)
        vertexDescriptor.attributes[2] = MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate,
                                                            format: .float2,
                                                            offset: MemoryLayout<Float>.size * 6,
                                                            bufferIndex: 0)
        vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * 8)
        return vertexDescriptor
    }
    
    
    static func buildSamplerState(device: MTLDevice) -> MTLSamplerState {
            let samplerDescriptor = MTLSamplerDescriptor()
            samplerDescriptor.normalizedCoordinates = true
            samplerDescriptor.minFilter = .linear
            samplerDescriptor.magFilter = .linear
            samplerDescriptor.mipFilter = .linear
            return device.makeSamplerState(descriptor: samplerDescriptor)!
    }

    
    static func buildDepthStencilState(device: MTLDevice) -> MTLDepthStencilState {
        let depthStencilDescriptor = MTLDepthStencilDescriptor()
        depthStencilDescriptor.depthCompareFunction = .less
        depthStencilDescriptor.isDepthWriteEnabled = true
        return device.makeDepthStencilState(descriptor: depthStencilDescriptor)!
    }
    
    
    static func buildScene(device: MTLDevice, vertexDescriptor: MDLVertexDescriptor) -> Scene {
        let scene = Scene()
        scene.ambientLightColor = SIMD3<Float>(1.0, 1.0, 1.0)   // temp was before 0.1 x 3
        let light0 = Light(worldPosition: SIMD3<Float>(100, 0, 0), color: SIMD3<Float>(0.1, 0.1, 0.1))
        let light1 = Light(worldPosition: SIMD3<Float>(0, 100, -100), color: SIMD3<Float>(0.1, 0.1, 0.1))
        let light2 = Light(worldPosition: SIMD3<Float>(0, -100, 100), color: SIMD3<Float>(0.1, 0.1, 0.1))
        scene.lights = [ light0, light1, light2 ]
        return scene
    }

    
    static func buildPipeline(device: MTLDevice, view: MTKView, vertexDescriptor: MDLVertexDescriptor) -> MTLRenderPipelineState {
        guard let library = device.makeDefaultLibrary() else {
            fatalError("Could not load default library from main bundle")
        }
        
        let vertexFunction = library.makeFunction(name: "vertex_main")
        let fragmentFunction = library.makeFunction(name: "fragment_main")
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        
        pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat
        pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat

        let mtlVertexDescriptor = MTKMetalVertexDescriptorFromModelIO(vertexDescriptor)
        pipelineDescriptor.vertexDescriptor = mtlVertexDescriptor
        
        do {
            return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            fatalError("Could not create render pipeline state object: \(error)")
        }
    }
    
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }


    // find an object in the list of all render scene children based on the name we gave the object
    func findNodeByName(_ name: String) -> Node? {
        for node in scene.rootNode.children {
            if node.name == name {
                return node
            }
        }
        return nil
    }

    
    // main render loop handler
    func draw(in view: MTKView) {
        let commandBuffer = commandQueue.makeCommandBuffer()!
        
        if let renderPassDescriptor = view.currentRenderPassDescriptor, let drawable = view.currentDrawable {
            let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
            
            time += 1 / Float(view.preferredFramesPerSecond)
            
            // rotation speed and direction
            let cameraAngle = -time / 2
            let globalAngle = Float(0)


            // for all objects, find their render node and add the rotation-per-cycle angle to the node angle
            // this makes all the labels to face the camera
            for a in SpaceObjects {
                let aNode = findNodeByName(a.nodeName)
                aNode!.a = aNode!.a + a.rotation
            }


            // we don't rotate the star field, we actually rotate the camera around the star field
            // rotate and move the camera
            let cameraAngleX = Float(0)
            let cameraAngleY = cameraAngle
            let cameraAngleZ = Float(0)
            cameraWorldPosition = SIMD3<Float>(Float(0), Float(0), Float(90))

            
            viewMatrix = float4x4(translationBy: -cameraWorldPosition) * float4x4(rotationAbout: SIMD3<Float>(1, 0, 0), by: cameraAngleX) * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: cameraAngleY) * float4x4(rotationAbout: SIMD3<Float>(0, 0, 1), by: cameraAngleZ)
            
            let aspectRatio = Float(view.drawableSize.width / view.drawableSize.height)
            projectionMatrix = float4x4(perspectiveProjectionFov: Float.pi / 6, aspectRatio: aspectRatio, nearZ: 0.1, farZ: 200)
            
            
            commandEncoder.setFrontFacing(.counterClockwise)
            commandEncoder.setCullMode(.back)
            commandEncoder.setDepthStencilState(depthStencilState)
            commandEncoder.setRenderPipelineState(renderPipeline)
            commandEncoder.setFragmentSamplerState(samplerState, index: 0)
            
            
            // loop through all the objects in the scene
            for m in scene.rootNode.children {
                // nudge each render node by its vectors
                // for the stars we don't use this - all the vectors are 0. But theoretically we could have object fly around 3D space under the control of the rendeerer here
                m.x = m.x + m.vx
                m.y = m.y + m.vy
                m.z = m.z + m.vz
                m.modelMatrix = matrix_identity_float4x4 * float4x4(translationBy: SIMD3<Float>(m.x, m.y, m.z)) * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: m.a) *  float4x4(scaleBy: m.s)
                
                // render all children of objects recursively (for this star system renderer, there are no children)
                drawNodeRecursive(m, parentTransform: matrix_identity_float4x4, commandEncoder: commandEncoder, globalAngle: globalAngle)
            }
                    
            
            commandEncoder.endEncoding()
            commandBuffer.present(drawable)
            commandBuffer.commit()
        }
    }
    
    
    func drawNodeRecursive(_ node: Node, parentTransform: float4x4, commandEncoder: MTLRenderCommandEncoder, globalAngle: Float) {
        let modelMatrix = parentTransform * node.modelMatrix * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: globalAngle)

        if let mesh = node.mesh, let baseColorTexture = node.material.baseColorTexture {
            // print("draw mesh: "+String(node.name))
            let viewProjectionMatrix = projectionMatrix * viewMatrix

            var vertexUniforms = VertexUniforms(viewProjectionMatrix: viewProjectionMatrix,
                modelMatrix: modelMatrix,
                normalMatrix: modelMatrix.normalMatrix)

            commandEncoder.setVertexBytes(&vertexUniforms, length: MemoryLayout<VertexUniforms>.size, index: 1)

            var fragmentUniforms = FragmentUniforms(cameraWorldPosition: cameraWorldPosition,
                ambientLightColor: scene.ambientLightColor,
                specularColor: node.material.specularColor,
                specularPower: node.material.specularPower,
                light0: scene.lights[0],
                light1: scene.lights[1],
                light2: scene.lights[2])

            commandEncoder.setFragmentBytes(&fragmentUniforms, length: MemoryLayout<FragmentUniforms>.size, index: 0)

            commandEncoder.setFragmentTexture(baseColorTexture, index: 0)

            let vertexBuffer = mesh.vertexBuffers.first!
            commandEncoder.setVertexBuffer(vertexBuffer.buffer, offset: vertexBuffer.offset, index: 0)

            for submesh in mesh.submeshes {
                let indexBuffer = submesh.indexBuffer
                commandEncoder.drawIndexedPrimitives(type: submesh.primitiveType,
                                                     indexCount: submesh.indexCount,
                                                     indexType: submesh.indexType,
                                                     indexBuffer: indexBuffer.buffer,
                                                     indexBufferOffset: indexBuffer.offset)
            }
        }

        for child in node.children {
            drawNodeRecursive(child, parentTransform: modelMatrix, commandEncoder: commandEncoder, globalAngle: globalAngle)
        }
    }

    
}
