//
//  tools.swift
//  1001 Stars
//
//  Created by Thomas Sturm on 11/26/22.
//

import Foundation
import MetalKit
import simd
import SwiftUI


struct Light {
    var worldPosition = SIMD3<Float>(0, 0, 0)
    var color = SIMD3<Float>(0, 0, 0)
}

struct VertexUniforms {
    var viewProjectionMatrix: float4x4
    var modelMatrix: float4x4
    var normalMatrix: float3x3
}

class Material {
    var specularColor = SIMD3<Float>(1, 1, 1)
    var specularPower = Float(1)
    var baseColorTexture: MTLTexture?
}

class Node {
    var name: String
    weak var parent: Node?
    var children = [Node]()
    var modelMatrix = matrix_identity_float4x4
    var mesh: MTKMesh?
    var material = Material()
    var a: Float = 0
    var s: Float = 0
    var x: Float = 0
    var y: Float = 0
    var z: Float = 0
    var vx: Float = 0
    var vy: Float = 0
    var vz: Float = 0
    init(name: String) {
        self.name = name
    }
    func nodeNamedRecursive(_ name: String) -> Node? {
        for node in children {
            if node.name == name {
                return node
            } else if let matchingGrandchild = node.nodeNamedRecursive(name) {
                return matchingGrandchild
            }
        }
        return nil
    }
}

// we keep all the original 3D models in the asset store
var NodeAssetStore = [Node]()


struct SpaceObject {
    var nodeName: String
    var rotation: Float
    var size: Float
    var type: String
}

// we keep references for any 3D models in the node
var SpaceObjects = [SpaceObject]()


struct Star {
    var NAME: String
    var VMAG: String
    var JMAG: String
    var DIST: String
    var X: String
    var Y: String
    var Z: String
    var TY: String
    var SPEC: String
    var MV: String
    var RADIUS: String
}

// this array holds the imported data from the CSV
var AllStars = [Star]()


func getCSVData() {
    do {
        let filename = Bundle.main.path(forResource: "stars-25ly-radius", ofType: "csv")!
        let content = try String(contentsOfFile: filename)
        let csvLines: [String] = content.components(
            separatedBy: "\n"
        )
        // we skip the first line, that's the field names
        for i in 1..<(csvLines.count-1) {
            let oneCSVRow = csvLines[i].components(separatedBy: ",")
            
            let a = Star(
                NAME: String(oneCSVRow[0]),
                VMAG: String(oneCSVRow[1]),
                JMAG: String(oneCSVRow[2]),
                DIST: String(oneCSVRow[3]),
                X: String(oneCSVRow[4]),
                Y: String(oneCSVRow[5]),
                Z: String(oneCSVRow[6]),
                TY: String(oneCSVRow[7]),
                SPEC: String(oneCSVRow[8]),
                MV: String(oneCSVRow[9]),
                RADIUS: String(oneCSVRow[10])
            )
            AllStars.append(a)
        }
    }
    catch {
        logger(logmsg: "CSV data failed")
    }
}


func logger(logmsg : String) {
    print(logmsg)
}

